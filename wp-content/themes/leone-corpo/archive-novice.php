<?php
/**
 * The template for displaying archive pages
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper novice__archive" id="archive-wrapper">
	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<?php
		    // left site menu
			$menu = get_terms(array(
				'taxonomy' => 'vrsta-novice',
				'hide_empty' => false,
			));
			/* d($menu);  */
			$kategorija = $_GET['vrsta-novice'];
		?>
		<div class="text-center content-block__title bgline__CentralShort">
			<h2 class="section-title">
				<?= get_field('archive_page_blacktitle', 'options'); ?> </h2>
			<h3 class="section-title section-title--red">
			<?= get_field('archive_page_redtitle', 'options'); ?></h3>
		</div>
		<?php if($menu): ?>
			<div class="category__list">
				<a class="category__item <?= $kategorija === null ? 'active' : ''; ?>" href="<?= get_post_type_archive_link('novice'); ?>">
					<?= __('Vse','Income'); ?>
				</a>
				<?php 
				foreach($menu as $item): ?>
					<a class="category__item <?= $item->slug === $kategorija ? 'active' : ''; ?>" href="<?= get_post_type_archive_link('novice'); ?>?vrsta-novice=<?= $item->slug; ?>">
						<?= $item->name; ?>
					</a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		<?php
		if ( have_posts() ) {
			?>
			<div class="novice__list">
			<?php
			// Start the loop.
			while ( have_posts() ) {
				the_post();
				?>
				<a class="novice__item" href="<?= get_the_permalink($item->ID); ?>">
					<div class="novice__date">
						<?= get_the_date('d. m. Y',$item->ID); ?>
					</div>
					<div class="novice__title">
						<?= get_the_title($item->ID); ?>
					</div>
					<div class="novice__img">
						<img src="<?= get_the_post_thumbnail_url($item->ID); ?>">
					</div>
					<div class="button button__primary" >
						<?= __('Preberi več', 'income'); ?>
					</div>
				</a>
				<?php
			}
			?>
			</div>
		<?php
		} else { ?>
			
			<?php
		}
		?>
	<?php
	// Display the pagination component.
	understrap_pagination();
	// Do the right sidebar check.
	?>
	</div><!-- #content -->
</div><!-- #archive-wrapper -->

<?php
get_footer();
