<?php

if (!function_exists('register_novice')) {
	function register_novice()
	{
		register_post_type( 'novice', array(
			'labels'             => array(
				'name'               => __( 'Novice', 'saleszone' ),
				'singular_name'      => __( 'Novica', 'saleszone' ),
				'add_new'            => __( 'Add a new', 'saleszone' ),
				'add_new_item'       => __( 'Add Novica', 'saleszone' ),
				'edit_item'          => __( 'Edit Novica', 'saleszone' ),
				'new_item'           => __( 'New Novica', 'saleszone' ),
				'view_item'          => __( 'View novica', 'saleszone' ),
				'search_items'       => __( 'Search for novica', 'saleszone' ),
				'not_found'          => __( 'Novica not found', 'saleszone' ),
				'not_found_in_trash' => __( 'No novice found in the trash', 'saleszone' ),
				'all_items'          => __( 'Vse novice', 'saleszone' ),
				'archives'           => __( 'Archives of novice', 'saleszone' ),
				'insert_into_item'   => __( 'Paste into novica', 'saleszone' ),
				'menu_name'          => __( 'Novice', 'saleszone' ),
				'items_list'         => __( 'Novica list', 'saleszone' ),
			),
			'public'             => true,
			'publicly_queryable' => true,
			'menu_position'      => null,
			'show ui'            => true,
			'menu_icon'          => 'dashicons-format-aside',
			'capability_type'    => 'post',
			'hierarchical'       => true,
			'taxonomies'         => array( 'vrsta-novice'),
			'has_archive'        => true,
			'rewrite'            => array( 'slug' => 'novice' ),
			'query_var'          => true,
			'show_in_rest'		 => true,
			'supports'           => array(
				'title',
				'editor',
				'thumbnail',
				'excerpt',
				'trackbacks',
				'custom-fields',
				'comments',
				'revisions',
				'page-attributes',
			),
		) );
		register_taxonomy(
			'vrsta-novice',
			'novice',
			array(
				'labels'       => array(
					'name'          => __( 'Vrsta novice', 'saleszone' ),
					'singular_name' => __( 'Vrsta novice', 'saleszone' ),
					'menu_name'     => __( 'Vrsta novice', 'saleszone' ),
					'all_items'     => __( 'All Vrsta novice', 'saleszone' ),
					'edit_item'     => __( 'Change Vrsta novice', 'saleszone' ),
					'view_item'     => __( 'View Vrsta novice', 'saleszone' ),
					'update_item'   => __( 'Update Vrsta novice', 'saleszone' ),
					'add_new_item'  => __( 'Add a new Vrsta novice to the gallery', 'saleszone' ),
					'search_items'  => __( 'Search Vrsta novice', 'saleszone' ),
					'not_found'     => __( 'No Vrsta novice found.', 'saleszone' ),
				),
				'hierarchical'      => true,
				'show_in_rest' =>true,
				'rewrite' => array(
					'slug' => 'vrsta-novice'
				),
			)
		);
	};

	add_action( 'init', 'register_novice' );
}

add_action( 'wp_dashboard_setup', 'remove_draft_widget', 999 );

function remove_draft_widget(){
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}
add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );

function remove_default_post_type_menu_bar( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'new-post' );
}
add_action( 'admin_menu', 'remove_default_post_type' );

function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
}

	
// remove_filter('the_content', 'wpautop');

// function WP_auto_formatting($content) {
//     global $post;
//     if(get_post_meta($post->ID, 'disable_auto_formatting', true) == 1) {
//         remove_filter('the_content', 'wpautop');
//     }
//     return $content;   
// }
// add_filter( "the_content", "WP_auto_formatting", 1 );