<?php
/**
 * Template Name: Content page
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


get_header();


get_template_part('page-templates/page-templates-part/main-flexible'); 

echo '<div class="container content-block pr-3 pl-3">';
    echo '<div class="row d-flex justify-content-center">';
        if (have_posts()):
            while (have_posts()) : the_post();
            echo '<div class="col-lg-8">';
                the_content();
            echo '</div>';
            endwhile;
        else:
            echo '<p>Sorry, no posts matched your criteria.</p>';
        endif;
    echo '</div>';
echo '</div>';

get_footer();
