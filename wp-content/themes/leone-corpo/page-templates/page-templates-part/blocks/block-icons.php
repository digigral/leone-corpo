<?php 
if(isset($args['block'])):
    $block = $args['block']; 
    $block_id = $args['block_id'];
    $target_blank = $args['target_blank'];
    $order = $block['order'];
    $order= explode(':', $order)[0];
?>
    <section id="<?= $block_id ?>" class="content-block  content-block__<?= $order; ?> <?= $order === 'rl'? 'bgline__HomeRight': 'bgline__HomeLeft'; ?>">       
        <div class="container">
            <div class="icons__block icons__block-<?= $order; ?>">
                <div class="icons__list">                    
                    <svg class="icons__item icons__item-first">                        
                        <use xlink:href="#svg_<?= $block['icon_1']['icons']; ?>" />
                    </svg>
                    <svg class="icons__item icons__item-second">
                        <use xlink:href="#svg_<?= $block['icon_2']['icons']; ?>" />
                    </svg>
                    <svg class="icons__item icons__item-third">
                        <use xlink:href="#svg_<?= $block['icon_3']['icons']; ?>" />
                    </svg>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-5  col-lg-6 pb-lg-0 <?= $order === 'rl'? 'order-md-1 order-lg-2':''; ?>">
                    <div class="d-flex flex-wrap w-100">
                        <h2 class="section-title mb-1 pr-lg-5 <?= $order === 'rl'? '':''; ?>">
                            <?= $block['title']; ?>
                        </h2>
                        <h3 class="section-title--red pr-3">
                            <?= $block['subtitle']; ?>
                        </h3>
                    </div>
                    
                    <div class="pb-3 d-md-none">
                        <img class="br-20 " src="<?= $block['image']; ?>" alt="">
                    </div>

                    <div class="content-block__content">
                        <?= $block['content']; ?>
                    </div>
                    <div class="content-block__buttons">
                        <?php 
                        if($block['buttons']):
                            foreach($block['buttons'] as $button): 
                                $button['button_style'] = explode(':', $button['button_style'])[0];                            
                            ?>
                                <a href="<?= $button['button_link'] ?>" <?php if($button['button_target_blank'] === true) echo 'target=__blank' ?> class="button button__<?= $button['button_style']; ?>"><?= $button['button_text']; ?></a>
                        <?php
                            endforeach;
                        endif; ?>
                    </div>     
                </div>
                <div class="col-xl-7 col-lg-6 d-flex align-items-center <?= $order === 'rl'? 'order-md-1 pr-4':'pl-lg-4'; ?>">
                    <img class="br-20 d-none d-lg-block" src="<?= $block['image']; ?>" alt="">
                </div>
            </div>
        </div>
    </section>
<?php
endif; ?>
    