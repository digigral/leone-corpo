<?php
if (isset($args['block'])) :
    $block = $args['block'];
    $block_id = $args['block_id'];
    $order = $block['order'];
    $order = explode(':', $order)[0];
?>
    <section id="<?= $block_id ?>" class="content-block  content-block__<?= $order; ?> ">
        <div class="container">
            <div class="text-center content-block__title">
                <div class="d-flex flex-wrap justify-content-center w-100">
                    <h2 class="section-title pr-3">
                        <?= $block['title']; ?>
                    </h2>
                    <h3 class="section-title section-title--red">
                        <?= $block['subtitle']; ?>
                    </h3>
                </div>    
            </div>
            <div class="text-center">
                <div class="content-block__content"><?= $block['content']; ?></div>
                <div class="content-block__buttons content-block__buttons-center">
                    <?php
                    if ($block['buttons']) :
                        foreach ($block['buttons'] as $button) :
                            $button['button_style'] = explode(':', $button['button_style'])[0];
                    ?>
                            <a href="<?= $button['button_link'] ?>" <?php if($button['button_target_blank'] === true) echo 'target=__blank' ?> class="button button__<?= $button['button_style']; ?>"><?= $button['button_text']; ?></a>
                    <?php
                        endforeach;
                    endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php
endif; ?>