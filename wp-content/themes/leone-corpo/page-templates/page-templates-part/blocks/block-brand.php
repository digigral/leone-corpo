<?php 
if(isset($args['block'])):
 $block = $args['block']; 
 $block_id = $args['block_id'];
 $order = $block['order'];
 $order= explode(':', $order)[0];

?>
    <section id="<?= $block_id ?>"  class="content-block content-block__brand  content-block__<?= $order; ?> <?= $order === 'rl'? 'bgline__HomeRight pt-3 pt-lg-0': 'bgline__HomeLeft'; ?>">
        <div class="container">
            <div class="row">
                <div class="col-xl-5  col-lg-6  pb-3 pb-lg-0  <?= $order === 'rl'? 'order-md-2':''; ?>  ">
                    <div>
                        <img class="content-block__brand-logo" src="<?= $block['title_logo']; ?>">
                    </div>
                    
					<h4 class="content-block__brand-title">
                        <?= $block['subtitle']; ?>
					</h3>

                    <div class="content-block__content">
                        <?= $block['description']; ?>
                    </div>

                    <div class="d-lg-none content-block__brand-bg content-block__brand-bg--<?= $block['background']; ?>">
                        <img class="" src="<?= $block['image']; ?>" alt="">
                    </div>

                    <div class="content-block__content pt-5 pt-lg-0">
                        <?= $block['content']; ?>
                    </div>
                    <div class="content-block__buttons">
                        <?php 
                        if($block['buttons']):
                            foreach($block['buttons'] as $button): 
                                $button['button_style'] = explode(':', $button['button_style'])[0];
                            ?>
                                <a href="<?= $button['button_link'] ?>" class="button button__<?= $button['button_style']; ?>"><?= $button['button_text']; ?></a>
                        <?php
                            endforeach;
                        endif; ?>
                    </div>     
                </div>
                <div class="col-xl-7 col-lg-6 d-flex flex-column<?= $order === 'rl'? 'order-md-1':'mt-lg-4'; ?> content-block__brand-bg content-block__brand-bg--<?= $block['background']; ?>">
                    <img class="d-none d-lg-block" src="<?= $block['image']; ?>" alt="">
                </div>
            </div>
        </div>
    </section>
<?php
endif; ?>
    