<?php
if (isset($args['block'])) :
    $block = $args['block'];
    $block_id = $args['block_id'];
    $order = $block['order'];
    $order = explode(':', $order)[0];
    $blocks = $block['blocks'];
    $icon = $block['icons_block'];
?>
    <section id="<?= $block_id ?>"  class="content-block  content-block__<?= $order; ?> bgline__CentralDouble">
        <div class="icons__block icons__block-centerd">
            <div class="icons__list">
                <svg class="icons__item icons__item-centerd">
                    <use  xlink:href="#svg_<?= $icon['icons']; ?>" />
                </svg>
            </div>
        </div>
        <div class="container">
            <div class="text-center content-block__title">
                <h2 class="section-title">
                    <?= $block['main_title']; ?>
                </h2>
                <h3 class="section-title section-title--red">
                    <?= $block['main_subtitle']; ?>
                </h3>
            </div>
            <?php
            if ($order === 'lr') :
                if ($blocks) :
                    foreach ($blocks as $key => $row) : ?>
                        <div class="row p-lg-5">
                            <div class="col-xl-5  col-lg-6  pb-3 pb-lg-0 d-flex align-items-start justify-content-center flex-column  <?= $key % 2 === 1 ? 'order-md-2' : '1'; ?>  ">
                                <h4><?= $row['title']; ?></h4>
                                <div class="content-block__content">
                                    <?= $row['content']; ?>
                                </div>
                                <div class="d-lg-none">
                                    <img class="br-20" src="<?= $row['image']; ?>" alt="">
                                </div>
                                <div class="content-block__buttons">
                                    <?php
                                    if ($blocks[0]["buttons"][0]) :
                                        foreach ($blocks[0]["buttons"][0] as $button) : 
                                            $button['button_style'] = explode(':', $button['button_style'])[0];      
                                        ?>
                                            <a href="<?= $button['button_link'] ?>" <?php if($button['button_target_blank'] === true) echo 'target=__blank' ?> class="button button__<?= $button['button_style']; ?>"><?= $button['button_text']; ?></a>
                                    <?php
                                        endforeach;
                                    endif; ?>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-6 d-flex justify-content-end  <?= $key % 2 === 1 ? 'order-md-1' : ''; ?>">
                                <img class="br-20" src="<?= $row['image']; ?>" alt="">
                            </div>
                        </div>
                    <?php endforeach;
                endif;
            else :
                if ($blocks) :
                    foreach ($blocks as $key => $row) : ?>
                        <div class="row  p-lg-5  <?= $key % 2 === 0 ? 'pt-3 pt-lg-0' : 'pt-5 pt-lg-0'; ?>">
                            <div class="col-xl-5 d-flex align-items-start justify-content-center flex-column col-lg-6  <?= $key % 2 === 0 ? 'order-md-1' : 'order-md-2'; ?>  ">
                                <h4><?= $row['title']; ?></h4>
                                <div class="content-block__content">
                                    <?= $row['content']; ?>
                                </div>
                                <div class="d-lg-none">
                                    <img class="br-20 " src="<?= $row['image']; ?>" alt="">
                                </div>
                                <div class="content-block__buttons">
                                    <?php

                                    if ($row["buttons"]) :
                                        foreach ($row["buttons"] as $button) : 
                                            $button['button_style'] = explode(':', $button['button_style'])[0];      
                                        ?>
                                            <a href="<?= $button['button_link'] ?>" <?php if($button['button_target_blank'] === true) echo 'target=__blank' ?> class="button button__<?= $button['button_style']; ?>"><?= $button['button_text']; ?></a>
                                    <?php
                                        endforeach;
                                    endif; ?>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-6 d-flex justify-content-end  <?= $key % 2 === 0 ? 'order-md-1' : ''; ?>">
                                <img class="br-20 d-none d-lg-block <?= $key % 2 === 0 ? '' : 'pb-5 pb-lg-0'; ?>" src="<?= $row['image']; ?>" alt="">
                            </div>
                        </div>
            <?php endforeach;
                endif;
            endif; ?>
        </div>
    </section>
<?php
endif; ?>