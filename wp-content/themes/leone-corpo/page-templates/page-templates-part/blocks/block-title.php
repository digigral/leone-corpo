<?php 
if(isset($args['block'])):
    $block = $args['block']; 
    $block_id = $args['block_id'];
    $order = $block['order'];
    $order= explode(':', $order)[0];
    $icon = $block['icons_block'];
?>
   <section id="<?= $block_id ?>" class="content-block  content-block__<?= $order; ?> bgline__one-l bgline__two-r bgline__CentralShort">
        <div class="icons__block icons__block-centerd">
            <div class="icons__list">
                <svg class="icons__item icons__item-centerd">
                    <use  xlink:href="#svg_<?= $icon['icons']; ?>" />
                </svg>
            </div>
        </div>
        <div class="container">
            <div class="text-center content-block__title">
                <h2 class="section-title">
                    <?= $block['title']; ?>
                </h2>
                <h3 class="section-title section-title--red">
                    <?= $block['subtitle']; ?>
                </h3>
            </div>
            <div class="d-lg-none">
                <img class="br-20 " src="<?= $block['image']; ?>" alt="">
            </div>
            <div class="row pt-3 pt-lg-0 p-lg-5">
                <div class="col-xl-5  col-lg-6  d-flex align-items-start justify-content-center flex-column <?= $order === 'rl'? 'order-md-2':''; ?>  ">
                    <h4><?= $block['block_tilte']; ?> </h4>
                    <div class="content-block__content">
                        <?= $block['content']; ?>
                    </div>
                    <div class="content-block__buttons">
                        <?php
                        if ($block['buttons']) :
                            foreach ($block['buttons'] as $button) : 
                                $button['button_style'] = explode(':', $button['button_style'])[0];      
                            ?>
                                <a href="<?= $button['button_link'] ?>" <?php if($button['button_target_blank'] === true) echo 'target=__blank' ?> class="button button__<?= $button['button_style']; ?>"><?= $button['button_text']; ?></a>
                        <?php
                            endforeach;
                        endif; ?>
                    </div> 
                </div>
                <div class="col-xl-7 col-lg-6 d-flex justify-content-end  <?= $order === 'rl'? 'order-md-1':''; ?>">
                    <img  class="br-20 d-none d-lg-block" src="<?= $block['image']; ?>" alt="">
                </div>
            </div>
        </div>
    </section>
<?php
endif; ?>
    