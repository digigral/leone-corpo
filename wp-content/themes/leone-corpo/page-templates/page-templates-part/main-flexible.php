<?php if (have_rows('flexible_content_main')) : ?>
    <div class="flexible__content">
        <?php while (have_rows('flexible_content_main')) : the_row(); ?>
            <?php if (get_row_layout() == 'content_block') : ?>
                <?php
                $block_id = get_sub_field('block_id');
                $block_type = get_sub_field('block_type', false);
                $block_type = explode(':', $block_type)[0];
                $block =  get_sub_field($block_type);
                $target_blank = get_sub_field('button_target_blank');

                get_template_part('page-templates/page-templates-part/blocks/block', $block_type, ['block' => $block, 'block_id' => $block_id, 'block_icons' => $block_icons, 'target_blank' => $target_blank]);
                ?>
                <!-- Section Here -->
            <?php elseif (get_row_layout() == 'hover_cards_and_text') : ?>
                <?php
                $order = get_sub_field('order');
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $content = get_sub_field('content');
                $read_more_text = get_sub_field('read_more_text');
                $read_more_link = get_sub_field('read_more_link');
                $target_blank = get_sub_field('button_target_blank');
                $cards = get_sub_field('cards');
                $id = get_sub_field('block_id');
                ?>
                <!-- Section Here -->

                <section  id="<?php  echo $id ?>" class="content-block <?= $order === 'rl'? 'bgline__HomeRight': 'bgline__HomeLeft'; ?>">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-5 col-lg-6">
                                <h2 class="section-title">
                                    <?= $title; ?>
                                </h2>
                                <h3 class="section-title--red">
                                    <?= $subtitle; ?>
                                </h3>
                                <div class="content-block__content">
                                    <?= $content; ?>
                                </div>
                                <?php if($read_more_link && $read_more_text): ?>
                                    <a href="<?= $read_more_link; ?>" class="button button__primary" <?php if($target_blank === true) echo 'target=__blank' ?>><?= $read_more_text; ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-xl-7 col-lg-6">
                                <div class="hover-card__list">
                                    <?php if ($cards) :
                                        foreach ($cards as $card) : 
                                            $icon = $card['title_icon']['icons'];
                                        ?>
                                            <div class="hover-card__item">
                                                <div class="hover-card__normal">
                                                    <?php if($icon !== 'noicon'): ?>
                                                    <div class="hover-card__normal-icon">
                                                        <svg>
                                                            <use xlink:href="#svg_<?= $icon; ?>" />
                                                        </svg>
                                                    </div>
                                                    <?php endif; ?>
                                                    <h4 class="hover-card__normal-title">
                                                        <?= $card['title']; ?>
                                                    </h4>
                                                    <div class="hover-card__more">+</div>
                                                </div>
                                                <?php if (is_front_page()) : ?>
                                                    <a class="hover-card__hover" href=" <?= $card['read_more_link']; ?>" <?php if($target_blank === true) echo 'target=__blank' ?>>
                                                <?php else: ?>     
                                                    <a class="hover-card__hover">
                                                <?php endif; ?>  
                                                    <div class="hover-card__hover-title">
                                                        <?php if($icon !== 'noicon'): ?>
                                                            <div class="hover-card__hover-icon">
                                                                <svg>
                                                                    <use xlink:href="#svg_<?= $icon; ?>" />
                                                                </svg>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?= $card['title']; ?>
                                                    </div>
                                                    <div class="hover-card__hover-content">
                                                        <?= $card['content']; ?>
                                                    </div>
                                                    <?php if (is_front_page()) : ?>
                                                        <div class="hover-card__hover-more"><img src="/wp-content/themes/leone-corpo/img/icons/arrow_right.svg" alt=""></div>
                                                    <?php endif; ?>    
                                                </a>
                                            </div>
                                    <?php
                                        endforeach;
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php elseif (get_row_layout() == 'images_list_whit_text') : ?>
                <?php
                $order = get_sub_field('order');
                $main_title = get_sub_field('main_title');
                $main_subtitle = get_sub_field('main_subtitle');
                $block_title = get_sub_field('block_title');
                $block_subtitle = get_sub_field('block_subtitle');
                $content = get_sub_field('content');
                $read_more_text = get_sub_field('read_more_text');
                $read_more_link = get_sub_field('read_more_link');
                $target_blank = get_sub_field('button_target_blank');
                $image_cards = get_sub_field('image_cards');
                $id = get_sub_field('block_id');
                
                ?>
                <!-- Section Here -->
                <section id="<?php  echo $id ?>" class="content-block images-text bgline__CentralShort">
                    <div class="container">
                        <div class="text-center content-block__title">
                            <h2 class="section-title">
                                <?= $main_title?>
                            </h2>
                            <h3 class="section-title section-title--red">
                                <?= $main_subtitle; ?>
                            </h3>
                        </div>
                        <div class="row images-text__content">
                            <div class="col-xl-5 col-lg-6 images-text__block">
                                <h2 class="images-text__block-title">
                                    <?= $block_title; ?>
                                </h2>
                                <h4 class="images-text__block-subtitle">
                                    <?= $block_subtitle; ?>
                                </h4>
                                <div class="content-block__content">
                                    <?= $content; ?>
                                </div>
                                <?php if($read_more_link && $read_more_text): ?>
                                    <a href="<?= $read_more_link; ?>" class="button button__primary"><?= $read_more_text; ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-xl-7 col-lg-6">
                                <div class="image_cards__list">
                                    <?php if ($image_cards) :
                                        foreach ($image_cards as $image) : 
                                            if($image['link']):
                                        ?>
                                                <a class="image_cards__item image_cards__item-link">
                                                    <img src="<?= $image['image'] ?>">
                                                </a>
                                            <?php 
                                            else: ?>
                                                <div class="image_cards__item">
                                                    <img src="<?= $image['image'] ?>">
                                                </div>
                                            <?php
                                            endif; ?>
                                    <?php
                                        endforeach;
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>                        
            <?php elseif (get_row_layout() == 'counter_list') : ?>
                <?php
                $background_of_section = get_sub_field('background_of_section');
                $first_block_title = get_sub_field('first_block_title');
                $bloks = get_sub_field('bloks');
                $id = get_sub_field('block_id');
                ?>
                <section id="<?php echo $id ?>" class="counter" style="background:url(<?php echo $background_of_section ?>);">
                    <div class="container">
                        <div class="counter__list">
                            <div class="counter__item counter__item--first">
                                <?= $first_block_title; ?>
                            </div>
                            <?php if ($bloks) :
                                foreach ($bloks as $block) : ?>
                                    <div class="counter__item">
                                        <div class="counter__item-number">
                                            <?= $block['nubmer']; ?>
                                        </div>
                                        <div class="counter__item-text">
                                            <?= $block['text']; ?>
                                        </div>
                                    </div>
                            <?php endforeach;
                            endif; ?>
                        </div>
                </section>
            <?php elseif (get_row_layout() == 'cards_list') : ?>
                <?php
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $cards = get_sub_field('cards');
                $subtext = get_sub_field('subtext');
                $uptext = get_sub_field('uptext');
                $buttons = get_sub_field('buttons');
                $id = get_sub_field('block_id');
                $icon = get_sub_field('icon_block');
                ?>
                <section class="cards bgline__CentralBig">
                    <div class="icons__block icons__block-centerd">
                        <div class="icons__list">
                            <svg class="icons__item icons__item-centerd">
                                <use  xlink:href="#svg_<?= $icon['icons']; ?>" />
                            </svg>
                        </div>
                    </div>
                    <div class="container">
                        <div class="text-center">
                            <h2 class="section-title">
                                <?= $title; ?>
                            </h2>
                            <h3 class="section-title--red">
                                <?= $subtitle; ?>
                            </h3>
                            <h4><?= $subtext ?></h4>
                        </div>
                        <div class="cards__list">
                            <?php if ($cards) :
                                foreach ($cards as $card) : 
                                    $icon = $card['title_icon']['icons'];
                                ?>
                                    <div class="cards__item">
                                        <div class="cards__item-title d-flex">
                                            <?php if($icon !== 'noicon'): ?>
                                                <svg class="cards__item-icon d-block">
                                                    <use xlink:href="#svg_<?= $icon; ?>" />
                                                </svg>
                                            <?php endif; ?>
                                            <?= $card['title'] ?>
                                        </div>
                                        <?php if ($card['lines']) : ?>
                                            <ul class="hooks__list">
                                                <?php foreach ($card['lines'] as $line) : ?>
                                                    <li class="hooks__item"><?= $line['line'] ?></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </div>
                            <?php endforeach;
                            endif; ?>
                        </div>
                        <div class="text-center cards__under">
                            <h4><?= $subtext ?></h4>
                            <div class="content-block__buttons">
                                    <?php
                                    if ($buttons) :
                                        foreach ($buttons as $button) : 
                                            $button['button_style'] = explode(':', $button['button_style'])[0];      
                                        ?>
                                            <a href="<?= $button['button_link'] ?>"<?php if($target_blank === true) echo 'target=__blank' ?> class="button button__<?= $button['button_style']; ?>"><?= $button['button_text']; ?></a>
                                    <?php
                                        endforeach;
                                    endif; ?>
                                </div>
                        </div>
                    </div>
                </section>

            <?php elseif (get_row_layout() == 'partners') : ?>
                <?php
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $border = get_sub_field('border');
                $partners_list = get_sub_field('partners_list');
                $read_more_link = get_sub_field('read_more_link');
                $read_more_text = get_sub_field('read_more_text');
                $id = get_sub_field('block_id');
                ?>
                <section id="<?php  echo $id ?>" class="partners bgline__CentralShort">
                    <div class="container">
                        <div class="partners__list__title">
                            <h1><?= $title; ?></h1>
                        </div>
                        <div class="partners__container">
                            <?php if($subtitle): ?>
                                <div class="partners__list__subtitle">
                                    <?= $subtitle; ?>
                                </div>
                            <?php endif; ?>
                            <div class="partners__list <?= $border ? 'partners__list-border' : ''; ?>">                          
                                <?php if ($partners_list) :
                                    foreach ($partners_list as $partner) : ?>
                                        <a class="partners__item" href="<?= $partner['link']; ?>">
                                            <img src="<?= $partner['logo']; ?>">
                                        </a>
                                <?php endforeach;
                                endif; ?>
                            </div>
                        </div>
                        <div class="text-center">
                            <?php if($read_more_link && $read_more_text): ?>
                                <a href="<?= $read_more_link; ?>" <?php if($target_blank === true) echo 'target=__blank' ?> class="button button__primary button-small"><?= $read_more_text; ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>


            <?php elseif (get_row_layout() == 'product_list') : ?>
                <?php
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $product_list = get_sub_field('product_list');
                $id = get_sub_field('block_id');
                $icon = get_sub_field('icon_block');
                ?>

                <section id="<?php  echo $id ?>" class="product-blocks bgline__CentralShort">
                    <div class="icons__block icons__block-centerd">
                        <div class="icons__list">
                            <svg class="icons__item icons__item-centerd">
                                <use  xlink:href="#svg_<?= $icon['icons']; ?>" />
                            </svg>
                        </div>
                    </div>
                    <div class="container">
                        <div class="text-center">
                            <h2 class="section-title">
                                <?= $title; ?>
                            </h2>
                            <h3 class="section-title--red">
                                <?= $subtitle; ?>
                            </h3>
                        </div>
                        <div class="product-blocks__list">
                            <?php
                            if ($product_list) :
                                foreach ($product_list as $prod) : ?>
                                    <div class="product-blocks__item">
                                        <div class="product-blocks__item-image">
                                            <img src="<?= $prod['image']; ?>">
                                        </div>
                                        <div class="product-blocks__item-title">
                                            <?= $prod['title']; ?>
                                        </div>
                                        <p class="product-blocks__item-content">
                                            <?= $prod['content']; ?>
                                        </p>
                                    </div>
                            <?php endforeach;
                            endif; ?>
                        </div>
                    </div>
                </section>
            <?php elseif (get_row_layout() == 'images_list') : ?>
                <?php
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $description = get_sub_field('description');
                $images_list = get_sub_field('images_list');
                $id = get_sub_field('block_id');
                ?>

                <section id="<?php  echo $id ?>" class="four-blocks four-blocks--images">
                    <div class="container">
                        <div class="text-center content-block__title">
                            <h2 class="section-title">
                                <?= $title; ?>
                            </h2>
                            <h3 class="section-title section-title--red">
                                <?= $subtitle; ?>
                            </h3>
                            <p class="pb-3 pb-lg-5"><strong><?= $description; ?></strong></p>
                        </div>
                        <div>
                            <div class="four-blocks__list">
                                <?php
                                if ($images_list) :
                                    foreach ($images_list as $img) : ?>
                                        <div class="four-blocks__item">
                                            <img src="<?= $img['image']; ?>">
                                        </div>
                                <?php endforeach;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </section>

            <?php elseif (get_row_layout() == 'three_blocks_link') : ?>
                <?php
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $blocks_list = get_sub_field('blocks_list');
                $background = get_sub_field('background');
                $id = get_sub_field('block_id');
                ?>
                <section id="<?php  echo $id ?>" class="three-blocks three-blocks--links three-blocks--<?= $background ?>">
                    <div class="container">
                        <div class="text-center">
                            <h2 class="section-title">
                                <?= $title; ?>
                            </h2>
                            <h3 class="section-title--red">
                                <?= $subtitle; ?>
                            </h3>
                        </div>
                        <div>
                            <div class="three-blocks__list">
                                <?php
                                if ($blocks_list) :
                                    foreach ($blocks_list as $block) : ?>
                                        <div class="three-blocks__item">
                                            <div class="three-blocks__image">
                                                <img src="<?= $block['image']; ?>">
                                            </div>                                            <?php if($block['read_more_link'] && $block['read_more_text']): ?>
                                                <a href="<?= $block['read_more_link']; ?>" class="button button__primary button-small" <?php if($target_blank === true) echo 'target=__blank' ?>><?= $block['read_more_text']; ?></a>
                                            <?php endif; ?>
                                        </div>
                                <?php endforeach;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </section>

            <?php elseif (get_row_layout() == 'three_cards_icons') : ?>
                <?php
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $icon_cards_list = get_sub_field('icon_cards_list');
                $id = get_sub_field('block_id');
                ?>
                <section id="<?php  echo $id ?>" class="three-blocks three-blocks--links three-blocks--<?= $background ?>">
                    <div class="container">
                        <div class="text-center content-block__title">
                            <h2 class="section-title">
                                <?= $title; ?>
                            </h2>
                            <h3 class="section-title section-title--red">
                                <?= $subtitle; ?>
                            </h3>
                        </div>
                        <div>
                            <div class="three-blocks__list">
                                <?php
                                if ($icon_cards_list) :
                                    foreach ($icon_cards_list as $block) : 
                                        $icon = $block['icon_block']['icons'];
                                    ?>
                                        <div class="three-blocks__card">
                                            <div class="three-blocks__card three-blocks__card--<?= $block['background']; ?>">
                                                <?php if($icon !== 'noicon'): ?>
                                                    <svg>
                                                        <use xlink:href="#svg_<?= $icon; ?>" />
                                                    </svg>
                                                <?php endif; ?>
                                                <div class="three-blocks__card-title">
                                                    <?=  $block['title']; ?>
                                                </div>
                                            </div>
                                            <div class="content-block__content">
                                                <?=  $block['content']; ?>
                                            </div>                                          
                                        </div>
                                <?php endforeach;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </section>

            <?php elseif (get_row_layout() == 'certificates') : ?>
                <?php
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $futured_row = get_sub_field('futured_row');
                $normal_row = get_sub_field('normal_row');
                $id = get_sub_field('block_id');
                $icon = get_sub_field('icon_block');
                ?>
                <section id="<?php  echo $id ?>" class="certificates bgline__CentralShort mt-lg-5">
                    <div class="icons__block icons__block-centerd">
                        <div class="icons__list">
                            <svg class="icons__item icons__item-centerd">
                                <use  xlink:href="#svg_<?= $icon['icons']; ?>" />
                            </svg>
                        </div>
                    </div>
                    <div class="container">
                        <div class="text-center">
                            <h2 class="section-title">
                                <?= $title; ?>
                            </h2>
                            <h3 class="section-title--red">
                                <?= $subtitle; ?>
                            </h3>
                        </div>
                        <div>
                            <div class="certificates__futured-row">
                                <?php
                                if ($futured_row) :
                                    foreach ($futured_row as $item) : ?>
                                        <?php if($item['link']): ?>
                                            <a href="<?= $item['link']; ?>" <?php if($target_blank === true) echo 'target=__blank' ?> class="certificates__item certificates__item-futured"> <img src="<?= $item['image']; ?>"></a>
                                        <?php else: ?>
                                            <div class="certificates__item certificates__item-futured">
                                                <img src="<?= $item['image']; ?>"></a>
                                            </div>
                                        <?php endif; ?>
                                <?php endforeach;
                                endif; ?>
                            </div>
                            <div class="certificates__normal-row">
                                <?php
                                if ($normal_row) :
                                    foreach ($normal_row as $item) : ?>
                                        <?php if($item['link']): ?>
                                            <a href="<?= $item['link']; ?>" <?php if($target_blank === true) echo 'target=__blank' ?> class="certificates__item certificates__item-normal"> <img src="<?= $item['image']; ?>"></a>
                                        <?php else: ?>
                                            <div class="certificates__item certificates__item-normal">
                                                <img src="<?= $item['image']; ?>"></a>
                                            </div>
                                        <?php endif; ?>
                                <?php endforeach;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </section>

            <?php elseif (get_row_layout() == 'icon_list') : ?>
                <?php
                $background = get_sub_field('background');
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $content = get_sub_field('content');
                $icon_list = get_sub_field('icon_list');
                $id = get_sub_field('block_id');
                $icon = get_sub_field('icon_block');
                ?>

                <section id="<?php  echo $id ?>" class="four-blocks four-blocks--icons bgline__CentralShort">
                    <div class="icons__block icons__block-centerd">
                        <div class="icons__list">
                            <svg class="icons__item icons__item-centerd">
                                <use  xlink:href="#svg_<?= $icon['icons']; ?>" />
                            </svg>
                        </div>
                    </div>
                    <div class="container">
                        <div class="four-blocks--bg-green">
                            <div class="text-center content-block__title">
                                <h2 class="section-title">
                                    <?= $title; ?>
                                </h2>
                                <h3 class="section-title section-title--red">
                                    <?= $subtitle; ?>
                                </h3>
                            </div>
                            <h4 class="text-center icons__special-tilte">
                                <?= $content; ?>
                            </h4>
                            <div class="four-blocks__list">
                                <?php
                                if ($icon_list) :
                                    foreach ($icon_list as $icon) :        
                                    ?>
                                        <div class="four-blocks__item">
                                            <?php if($icon !== 'noicon'): ?>
                                                <div class="four-blocks__item--icon">
                                                    <svg class="">
                                                        <use xlink:href="#svg_<?= $icon['icon']['icons']; ?>" />
                                                    </svg>
                                                </div>
                                            <?php endif; ?>
                                            <div class="four-bloks__item--content">
                                                <?= $icon['content']; ?>
                                            </div>
                                        </div>
                                <?php endforeach;
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </section>

            <?php elseif (get_row_layout() == 'map_image') : ?>
                <?php
                $background = get_sub_field('background');
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $content = get_sub_field('content');
                $image = get_sub_field('image');
                $id = get_sub_field('block_id');
                ?>

                <section id="<?php  echo $id ?>" class="map-image pt-5 pt-lg-0">
                    <div class="container">
                        <div>
                            <div class="text-center">
                                <h2 class="section-title">
                                    <?= $title; ?>
                                </h2>
                                <h3 class="section-title--red">
                                    <?= $subtitle; ?>
                                </h3>
                                <p class="map-image__special-content">
                                    <?= $content; ?>
                                </p>
                            </div>
                            <div class="map-image__content map-image--<?= $background; ?>">
                               <img src="<?= $image; ?>">
                            </div>
                        </div>
                    </div>
                </section>        

            <?php elseif (get_row_layout() == 'map_image') : ?>
                <?php
                $order = get_sub_field('order');
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $content = get_sub_field('content');
                $image = get_sub_field('image');
                $id = get_sub_field('block_id');
                ?>

                <section id="<?php  echo $id ?>" class="quote-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 quote-block__right">
                                <img class="quote-block__image" src="<?= $image; ?>">
                            </div>
                            <div class="col-md-8 quote-block__left">
                                <div class="quote-block__quote">
                                    <?= $content; ?>
                                </div>
                                <h3 class="quote-block__title">
                                    <?= $title; ?>
                                </h3>

                                <div class="quote-block__subtitle">
                                    <?= $subtitle; ?>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </section>         

            <?php elseif (get_row_layout() == 'contact_block') : ?>
                <?php
                $order = get_sub_field('order');
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $logo = get_sub_field('logo');
                $tile_of_logo = get_sub_field('tile_of_logo');
                $contact_list = get_sub_field('contact_list');
                $contact_tile = get_sub_field('contact_tile');
                $contact_form_id = get_sub_field('contact_form_id');
                $id = get_sub_field('block_id');
                ?>

                <section id="<?php  echo $id ?>" class="contact-block">
                    <div class="container">
                        <div class="text-center content-block__title">
                            <h2 class="section-title">
                                <?= $title; ?>
                            </h2>
                            <h3 class="section-title section-title--red">
                                <?= $subtitle; ?>
                            </h3>
                        </div>
                        <div class="row contact-block__content">
                            <div class="col-md-6 contact-block__right">
                                <img class="contact-block__logo" src="<?= $logo; ?>">
                                <div class="contact-block__titlelogo">
                                    <?= $tile_of_logo; ?>
                                </div>
                                <?php 
                                if($contact_list): ?>
                                    <ul class="contact-block__list">
                                        <?php 
                                        foreach($contact_list as $contact): 
                                            
                                        ?>
                                            <li class="contact-block__item">
                                                <?php 
                                                if($contact['link_row']): ?>
                                                    <a href="<?= $contact['link']; ?>">
                                                        <?php if($contact['icon_block']['icons'] !== 'noicon'): ?>
                                                            <svg class="">
                                                                <use xlink:href="#svg_<?= $contact['icon_block']['icons']; ?>" />
                                                            </svg>
                                                        <?php endif; ?>
                                                        <?= $contact['text']; ?>
                                                    </a>
                                                <?php
                                                else: ?>
                                                    <?php if($contact['icon_block']['icons'] !== 'noicon'): ?>
                                                   <svg class="">
                                                        <use xlink:href="#svg_<?= $contact['icon_block']['icons']; ?>" />
                                                    </svg>
                                                    <?php endif; ?>
                                                    <?= $contact['text']; ?>
                                                <?php 
                                                endif; ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php
                                endif; ?>
                            </div>
                            <div class="col-md-6 contact-block__left">
                                <div class="contact-block__title">
                                    <?= $contact_tile; ?>
                                </div>                      
                                <?= do_shortcode($contact_form_id); ?>
                        </div>                        
                    </div>
                </section>
            <?php elseif (get_row_layout() == 'contact_form_block') : ?>
                <?php 
                    $title = get_sub_field('title');
                    $subtitle = get_sub_field('subtitle');
                    $contact_form_id = get_sub_field('contact_form_id');
                    $id = get_sub_field('block_id');    
                ?>   

                <section id="<?php  echo $id ?>" class="contact-block">
                    <div class="container">
                        <div class="row contact-block__content justify-content-center">
                            <div class="col-12">
                            <div class="text-center content-block__title mb-4">
                                <h2 class="section-title">
                                    <?= $title; ?>
                                </h2>
                                <h3 class="section-title section-title--red">
                                    <?= $subtitle; ?>
                                </h3>
                            </div>
                            </div>
                            <div class="col-xl-6 contact-block__left">
                                <?= do_shortcode($contact_form_id); ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php elseif (get_row_layout() == 'timeline') : ?>
                <section class="content-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div>
                                    <?php 
                                        $title = get_sub_field('title');
                                        $subtitle = get_sub_field('subtitle');
                                    ?>
                                     <h2 class="section-title mb-1 pr-lg-5 text-center"><?= $title ?></h2>
                                     <h3 class="text-center"><?= $subtitle ?></h3>
                                </div>
                                <div class="timeline__list">
                                    <?php 
                                        $timeline = get_sub_field('timeline');
                                        if($timeline):
                                            foreach($timeline as $key=>$line):
                                        ?>
                                                <div class="timeline__item timeline__item-<?= $key%2 > 0 ? 'even':'odd'; ?>">
                                                    <div class="timeline__item__description">
                                                        <h3 class="timeline__item__description__title">
                                                            <?= $line['title']; ?>
                                                        </h3>
                                                        <div class="timeline__item__description__content">
                                                            <?= $line['text']; ?>
                                                        </div>                            
                                                    </div>
                                                    <div class="timeline__item__year">
                                                        <?= $line['year']; ?>
                                                    </div>
                                                </div>
                                            <?php
                                            endforeach; 
                                        endif;
                                        ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php elseif (get_row_layout() == 'contact_address') : ?>
                <?php
                $title = get_sub_field('title');
                $address_list = get_sub_field('address_list');
                $id = get_sub_field('block_id');
                ?>

                <section id="<?php  echo $id ?>" class="contact-address">
                    <div class="container">
                        <h3 class="contact-address__title">
                            <?= $title; ?>
                        </h3>
                        <?php 
                        if($address_list): ?>
                        <div class="contact-address__list">
                            <?php 
                            foreach($address_list as $address): ?>
                                <div class="contact-address__item">
                                    <div class="contact-address__item-title">
                                        <?= $address['address_title']; ?>
                                    </div>
                                    <?php
                                    if($address['contact_list']): ?>
                                        <div class="contact-address__item-list">
                                            <?php
                                            foreach($address['contact_list'] as $contact): ?>
                                                <div class="contact-address__contact">
                                                    <?php 
                                                    if($contact['link_row']): ?>
                                                        <a href="<?= $contact['link']; ?>">
                                                            <?php if($contact['icon_block']['icons'] !== 'noicon'): ?>
                                                                <svg class="">
                                                                    <use xlink:href="#svg_<?= $contact['icon_block']['icons']; ?>" />
                                                                </svg>
                                                            <?php endif; ?>
                                                        <?= $contact['text']; ?>  
                                                        </a>
                                                    <?php
                                                    else: ?>
                                                        <?php if($contact['icon_block']['icons'] !== 'noicon'): ?>
                                                            <svg class="">
                                                                <use xlink:href="#svg_<?= $contact['icon_block']['icons']; ?>" />
                                                            </svg>
                                                        <?php endif; ?>
                                                        <?= $contact['text']; ?>
                                                    <?php 
                                                    endif; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d289887.8744375712!2d13.765706799714811!3d45.987751881466586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x477b193b3793345f%3A0xe5f8eca9d4c468bc!2sIncom%20proizvodno%20trgovsko%20podjetje%20d.o.o.%20Ajdov%C5%A1%C4%8Dina-Maloprodaja%2C%20Ajdov%C5%A1%C4%8Dina!5e0!3m2!1sen!2ssi!4v1618562760710!5m2!1sen!2ssi" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>                           
                    </div>
                </section>        

            <?php elseif (get_row_layout() == 'title') : ?>
                <?php
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $type = get_sub_field('type');
                $id = get_sub_field('block_id');
                ?>
                <section id="<?php  echo $id ?>" class="title-block">
                    <div class="container">
                        <div class="text-center <?= $type === 'oneline' ? 'content-block__title' : ''?>">
                            <h2 class="section-title">
                                <?= $title; ?> 
                            </h2>
                            <h3 class="section-title section-title--red">
                                <?= $subtitle; ?>
                            </h3>
                        </div>
                    </div>
                </section>     
            <?php elseif (get_row_layout() == 'quote_block') : ?>
                <?php
                $order = get_sub_field('order');
                $image = get_sub_field('image');
                $content = get_sub_field('content');
                $title = get_sub_field('title');
                $subtitle = get_sub_field('subtitle');
                $id = get_sub_field('block_id');
    
                $signature = get_sub_field('signature');
                ?>
                <section id="<?php  echo $id ?>" class="quote-block">
                    <div class="container  quote-block__container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="quote-block__img">
                                    <img src="<?= $image; ?>">
                                </div>
                            </div>
                            <div class="col-md-8 quote-block__content">
                                <div>
                                    <div class="quote-block__quote">
                                        <?= $content; ?>
                                    </div>
                                    <div class="quote-block__title">
                                        <?php if($title): ?>
                                            <?= $title; ?>
                                        <?php else: ?>
                                            <img src="<?= $signature ?>" />
                                        <?php endif; ?>

                                    </div>
 
                                    <div class="quote-block__subtitle">
                                        <?= $subtitle; ?>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </section>  
            <?php elseif (get_row_layout() == 'image') : ?>
                <?php
                $id = get_sub_field('block_id');
                $image = get_sub_field('image');
                ?>

                <section  id="<?php  echo $id ?>" class="image-block">
                    <div class="container">
                        <div class="row">
                            <img src="<?php echo $image ?>"/>
                        </div>
                    </div>
                </section>
            <?php elseif (get_row_layout() == 'iframe_block') : ?>
                <?php
                    $iframe = get_sub_field('iframe');
                ?>
                <section class="iframe-block bgline__CentralShort">
                    <div class="container">
                        <div class="row">
                            <?= $iframe ?>
                        </div>
                    </div>
                </section>
            <?php elseif (get_row_layout() == 'padding_box') : ?>
                <?php
                    $size = get_sub_field('size');
                ?>
                <section class="padding_box" style="height: <?php echo $size ?>px;">
                    <div class="container">
                        <div class="row">
                        </div>
                    </div>
                </section>
        <?php elseif (get_row_layout() == 'accordion_block') : ?>    
                <?php
                    $tabs = get_sub_field('accordion_tabs');
                ?>
                <section class="accordion-tabs">
                    <div class="container">
                        <div class="row">
                            <div class="col-8">
                                <?php foreach($tabs as $tab): ?>
                                    <button class="accordion"><?= $tab['accordion_title']; ?></button>
                                    <div class="panel">
                                    <?= $tab['accordion_text']; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>  
                </section>  
            <?php elseif (get_row_layout() == 'html_block') : ?>
                <?php
                    $html = get_sub_field('html_content');
                ?>        
                <section class="html-content content-block">
                    <div class="conainer">
                        <div class="row">
                            <div class="col-12 text-center d-flex justify-content-center">
                                <?= $html ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
        <?php endwhile; ?>
    </div>
<?php endif; ?>