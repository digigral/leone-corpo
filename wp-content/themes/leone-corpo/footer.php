<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper footer" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">
			<div class="col-lg-2 footer__item">
				<img src="<?php echo get_field('footer_logo', 'options'); ?>" />
			</div>
			<div class="col-lg-3 footer__item">
				<h3 class="footer__menu-title"><?php echo __('Incom d.o.o.', 'leone'); ?></h3>
				<?php echo get_field('footer_about_text', 'options'); ?>
			</div>
			<div class="col-lg-3 footer__item">
				<strong>
					<h3 class="footer__menu-title"><?php echo __('Leone.si', 'leone'); ?></h3>
				</strong>
				<div class="footer__menu">
				<?php
					wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'container_class' => 'collapse navbar-collapse',
							'container_id'    => 'primary-menu',
							'menu_class'      => 'navbar-nav ml-auto',
							'fallback_cb'     => '',
							'menu_id'         => 'main-menu',
							'depth'           => 0,
							'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
						)
					);
				?>
				</div>
			</div>
			<div class="col-lg-3 footer__item">
				<strong>
					<h3 class="footer__menu-title"><?php echo __('Contacts', 'leone'); ?></h3>
				</strong>
				<?php echo get_field('footer_contact_text', 'options'); ?>

				<div class="footer__social">
					<?php 
					$soc = get_field('footer_social', 'options'); 
					if($soc):
						foreach($soc as $item):
					?>
							<a href="<?= $item['footer_social_link']; ?>">
								<svg><use xlink:href="#svg_<?= $item['footer_social_icon']['icons']; ?>" /></svg>
							</a>
					<?php 
						endforeach;
					endif; ?>
				</div>
			</div>
			<div class="col-lg-1 footer__item">
				<?php echo do_action('wpml_add_language_selector'); ?>
			</div>

		</div>

	</div><!-- container end -->

</div><!-- wrapper end -->

<div class="copyright">
	<div class="container">
		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

						<p class="m-0 text-center"><?php echo get_field('footer_copyright_text', 'options'); ?>  | <a href="<?php echo get_field('footer_privacy_policy', 'options') ?>"><?php echo __('Privacy Policy', 'leone'); ?></a> | <a href="<?php echo get_field('footer_terms_conditions', 'options') ?>"><?php echo __('Terms&Conditions', 'leone'); ?></a></p>

					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->
	</div>
</div>

</div><!-- #page we need this extra closing tag here -->

<?php get_template_part('page-templates/page-templates-part/symbols');
wp_footer(); ?>

</body>

</html>

