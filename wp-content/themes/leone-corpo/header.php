<?php

/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;



?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	
    <link rel="icon" href="/wp-content/themes/leone-corpo/img/favicon.ico">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>>
	<?php do_action('wp_body_open'); ?>
	<div class="site" id="page">

		<!-- ******************* The Navbar Area ******************* -->
		<div id="wrapper-navbar">
		<?php if (is_front_page()) : ?>
				<nav id="main-nav" class="navbar navbar--dark navbar-expand-md bg-dark-with-opacity" aria-labelledby="main-nav-label">
			<?php else : ?>
					<nav id="main-nav" class="navbar navbar--light navbar-expand-md bg-light" aria-labelledby="main-nav-label">
			<?php endif; ?>
					<div class="container">
						<div class="d-flex flex-row justify-content-between">
							<div class="">
								<div class="navbar-brand mb-0">
									<a rel="home" href="<?php echo esc_url(home_url('/')); ?>" itemprop="url">
										<img src="/wp-content/themes/leone-corpo/img/logo.svg" alt="Leone">
									</a>
								</div>
							</div>
							<div class="desktop-menu flex-fill align-items-center justify-content-between">
								<div class="primary-menu">
									<?php
									wp_nav_menu(
										array(
											'theme_location'  => 'primary',
											'container_class' => 'collapse navbar-collapse',
											'container_id'    => 'primary-menu',
											'menu_class'      => 'navbar-nav ml-auto',
											'fallback_cb'     => '',
											'menu_id'         => 'main-menu',
											'depth'           => 0,
											'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
										)
									);
									?>
								</div>
								<div class="secondary-menu d-flex align-items-center">
									<?php
									wp_nav_menu(
										array(
											'theme_location'  => 'contact-us',
											'container_class' => 'collapse navbar-collapse',
											'container_id'    => 'contact-us-menu',
											'menu_class'      => 'navbar-nav ml-auto',
											'fallback_cb'     => '',
											'menu_id'         => 'main-menu',
											'depth'           => 2,
											'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
										)
									);
									?>
									<?php echo do_action('wpml_add_language_selector'); ?>
								</div>
							</div>
							<div class="d-flex align-items-center">
								<div id="ham-icon" class="ham-icon">
									<span class="ham-icon__top"></span>
									<span class="ham-icon__middle"></span>
									<span class="ham-icon__bottom"></span>
								</div>
							</div>
						</div>
						<!-- end custom logo -->

					</div>
				</nav><!-- .site-navigation -->	

				<?php if (is_front_page()) : ?>
					<div id="mobile-nav" class="mobile-nav navbar navbar--dark bg-dark-with-opacity">
				<?php else : ?>
					<div id="mobile-nav" class="mobile-nav navbar navbar--light bg-light">
				<?php endif; ?>	
					<div class="primary-menu">
						<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => 'collapse navbar-collapse',
								'container_id'    => 'primary-menu',
								'menu_class'      => 'navbar-nav ml-auto',
								'fallback_cb'     => '',
								'menu_id'         => 'main-menu',
								'depth'           => 0,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						);
						?>
					</div>
					<div class="secondary-menu">
						<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'contact-us',
								'container_class' => 'collapse navbar-collapse',
								'container_id'    => 'contact-us-menu',
								'menu_class'      => 'navbar-nav ml-auto',
								'fallback_cb'     => '',
								'menu_id'         => 'main-menu',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							)
						);
						?>
						<div class="text-center">
							<?php echo do_action('wpml_add_language_selector'); ?>
						</div>
					</div>	
				</div>

			<!-- <a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e('Skip to content', 'understrap'); ?></a> -->
			<?php get_template_part('global-templates/hero', 'hero'); ?>
		</div><!-- #wrapper-navbar end -->