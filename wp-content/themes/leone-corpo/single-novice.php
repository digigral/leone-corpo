<?php
/**
 * The template for displaying all single posts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper novice novice__single">
	<div class="container" id="content" tabindex="-1">
		<div class="row">
			<div class="col-md-8 pb-5 pb-md-0 pr-md-5">
				 <h2 class="novice__single__title"><?= get_the_title(); ?></h2>
				 <div class="novice-content">
					 <?= the_content(); ?>
				 </div>
			</div>
			<div class="col-md-4">
				<h3 class="novice-last-news"> <?= __('Zadnje novice', 'income'); ?></h3>
				<?php 
				$args = array(
					'post_type' => 'novice',
					'post_per_page' => 3,
					'orderby' => 'date',
					'order'   => 'DESC',
					'post__not_in' => [$post->ID]
				);				
				$novice = new WP_Query($args);
				if($novice->posts):
				?>
				<div class="novice__single__list">
					<?php 
					foreach($novice->posts as $item): ?>
						<a class="novice__item" href="<?= get_the_permalink($item->ID); ?>">
							<div class="novice__date">
								<?= get_the_date('d. m. Y',$item->ID); ?>
							</div>
							<div class="novice__title">
								<?= get_the_title($item->ID); ?>
							</div>
							<div class="novice__img">
								<img src="<?= get_the_post_thumbnail_url($item->ID); ?>">
							</div>
							<div class="button button__primary" >
								<?= __('Preberi več', 'income'); ?>
							</div>
						</a>
					<?php endforeach; ?>
				</div>
				<?php endif; ?>
				<a class="novice__single__all" href="<?= get_post_type_archive_link('novice'); ?>">
					<span><?= __('back to all news', 'income'); ?></span>
					</a>
			</div>
		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #single-wrapper -->

<?php
get_footer();
