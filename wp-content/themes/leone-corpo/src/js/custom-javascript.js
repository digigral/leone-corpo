// Add your JS customizations here

// Mobile menu
let ham = document.getElementById("ham-icon");
let nav = document.getElementById("mobile-nav");
let navbar = document.getElementById("main-nav");

ham.onclick = function(){
    nav.classList.toggle("open");
    navbar.classList.toggle("open");
    ham.classList.toggle("open");
};


// Awards slider
jQuery('.awards-line').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  mobileFirst: true,
  responsive: [
    {
       breakpoint: 1200,
       settings: "unslick"
    }
 ],
 nextArrow: '<div class="slick-arrow right-arrow"></div>',
 prevArrow: '<div class="slick-arrow left-arrow"></div>',
});

// Awards slider
jQuery('.partners__list ').slick({
   slidesToShow: 2,
   slidesToScroll: 1,
   autoplay: true,
   autoplaySpeed: 2000,
   mobileFirst: true,
   responsive: [
     {
        breakpoint: 1200,
        settings: "unslick"
     }
  ],
  nextArrow: '<div class="slick-arrow right-arrow"></div>',
  prevArrow: '<div class="slick-arrow left-arrow"></div>',
 });


// Hover
if (window.matchMedia('(max-width: 768px)').matches)
{
   jQuery('.hover-card__item').on('click', (e) => {
      e.preventDefault();
   });
   
   jQuery('.hover-card__hover-more').on('click', (e) => {
      e.stopPropagation();
      $( ".hover-card__item" ).trigger( "click" );
   });
}

jQuery('#uploadcv-btn').on('click', function() {
   jQuery('.upload-cv').trigger('click');
});

jQuery('.upload-cv').change(function() {
   var fullPath = jQuery(this).val();
   if (fullPath) {
      var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
      var filename = fullPath.substring(startIndex);
      if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
          filename = filename.substring(1);
      }
      jQuery('.filename').val(filename);
  }
 });


// Accordion
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}