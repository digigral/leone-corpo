<?php 
	$awards = get_field('awards_items');
?>

<?php if( have_rows('awards_items') ): ?>
	<div class="container awards-line bg-white align-items-center justify-content-between">
		<?php while( have_rows('awards_items') ): the_row(); 
			$image = get_sub_field('award_image');
			$link = get_sub_field('award_link');
			?>

			<div class="awards-line__item d-flex align-items-center justify-content-center">
				<a href="<?php echo $link ?>">
					<img src="<?php echo $image; ?>">
				</a>
			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
