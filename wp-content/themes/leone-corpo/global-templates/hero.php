<?php
/**
 * Hero setup
 *
 * @package UnderStrap
 */

// Exit if accessed directly. 	
defined( 'ABSPATH' ) || exit;
$post_type = get_query_var('post_type');;
?>

<?php if (is_front_page()) : ?>
	
<header class="header align-items-center justify-content-center flex-column">
	<?php if(get_field('header_image', 'options')): ?>
		<img class="header__image d-none d-sm-block" src="<?= get_field('header_image', 'options'); ?>" alt="">
	<?php endif; ?>

	<?php if(get_field('header_mobile-image', 'options')): ?>
		<img class="header__image d-sm-none" src="<?= get_field('header_mobile-image', 'options'); ?>" alt="">
	<?php endif; ?>

	<!-- <?php if(get_field('header_video_id', 'options')): ?>
		<div class="header-unit">
			<div class="header-unit__overlay"></div>
			<div id="player"></div>
			<iframe class="fill-width" src="https://www.youtube.com/embed/<?= get_field('header_video_id', 'options'); ?>?autoplay=1&mute=1&autohide=2&modestbranding=1&fs=0&showinfo=0&rel=0&controls=0&loop=1&playlist=<?= get_field('header_video_id', 'options'); ?>" frameborder="0"></iframe><div style='font-size: 0.8em'>
		</div>
	<?php endif; ?> -->
		
	<div class="text-center header__content">
		<h1 class="main-title text-light">
			<?php if(get_field('page_title', 'options')): ?>
				<?= get_field('page_title', 'options'); ?>
			<?php endif; ?>
			
			<?php if(get_field('page_subtitle', 'options')): ?>
				<span class="main-title--span d-block">
					<?= get_field('page_subtitle', 'options'); ?>
				</span>
			<?php endif; ?>
		</h1>

		<?php if(get_field('page_description')): ?>
			<p class="text-light page-description">
				<?= get_field('page_description', 'options'); ?>
			</p>
		<?php endif; ?>

		<div class="buttons-group justify-content-center container-fluid">
			<div class="d-flex flex-wrap justify-content-center">
				<a href="<?= get_field('header_primary_link', 'options'); ?>" class="button button__primary" ><?= get_field('header_primary_button', 'options'); ?></a>
				<a href="<?= get_field('header_secondary_link', 'options'); ?>" class="button button__secondary"><?= get_field('header_secondary_button', 'options'); ?></a>
			</div>
		</div>
	</div>
</header>	
<?php elseif($post_type === 'novice'): ?>
	<header class="subpage-header hero-line d-flex align-items-center justify-content-center flex-column" style="background-image: url(/wp-content/themes/leone-corpo/img/4-456.jpg);">
		<div class="text-center col subpage-header__text">
			<h1 class="main-title text-light"><?= __('Novice','income'); ?></h1>			
			<?php if(get_field('page_subtitle_news', 'options')): ?>
				<h3 class="page-subtitle text-light"><?= get_field('page_subtitle_news', 'options'); ?></h3>
			<?php endif; ?>			
			<?php if(get_field('page_description_news', 'options')): ?>
				<p class="page-description text-light"><?= get_field('page_description_news', 'options'); ?></p>
			<?php endif; ?>
		</div>
	</header>					
<?php else : ?>
	<header class="subpage-header hero-line d-flex align-items-center justify-content-center flex-column" style="background-image: url(<?= get_field('header_animation'); ?>);">
		<div class="icons__block icons__block-header">
			<div class="icons__list">                    
				<svg class="icons__item icons__item-first">                        
					<use xlink:href="#svg_<?= get_field('icon_1_icons'); ?>" />
				</svg>
				<svg class="icons__item icons__item-second">
					<use xlink:href="#svg_<?= get_field('icon_2_icons'); ?>" />
				</svg>
				<svg class="icons__item icons__item-third">
					<use xlink:href="#svg_<?= get_field('icon_3_icons'); ?>" />
				</svg>
			</div>
		</div>
		<div class="text-center col subpage-header__text">
			<h1 class="main-title text-light"><?= the_title(); ?></h1>
			
			<?php if(get_field('page_subtitle')): ?>
				<h3 class="page-subtitle text-light"><?= get_field('page_subtitle'); ?></h3>
			<?php endif; ?>
			
			<?php if(get_field('page_description')): ?>
				<p class="page-description text-light"><?= get_field('page_description'); ?></p>
			<?php endif; ?>
		</div>
	</header>					
<?php endif; ?>





